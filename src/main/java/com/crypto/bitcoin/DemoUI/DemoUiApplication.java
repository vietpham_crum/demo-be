package com.crypto.bitcoin.DemoUI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DemoUiApplication{

	public static void main(String[] args) {
		SpringApplication.run(DemoUiApplication.class, args);
	}
}

