package com.crypto.bitcoin.DemoUI;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.*;

@Getter @Setter
@NoArgsConstructor
@ToString @EqualsAndHashCode
/**
 * Created by CUONTO on 25-5-2018.
 */
public class Number implements Serializable{

    BigDecimal absTotalProfit;
    BigDecimal absDailyProfit;
    BigDecimal relTotalProfit;
    BigDecimal relDailyProfit;
    BigDecimal baseToCounterPrice;
    BigDecimal counterToUSDPrice;

    public Number(BigDecimal absTotalProfit, BigDecimal absDailyProfit, BigDecimal relTotalProfit,
                  BigDecimal relDailyProfit, BigDecimal baseToCounterPrice, BigDecimal counterToUSDPrice){
    this.absTotalProfit = absTotalProfit;
    this.absDailyProfit = absDailyProfit;
    this.relTotalProfit = relTotalProfit;
    this.relDailyProfit = relDailyProfit;
    this.baseToCounterPrice = baseToCounterPrice;
    this.counterToUSDPrice = counterToUSDPrice;
    }
}
