package com.crypto.bitcoin.DemoUI;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.openhft.chronicle.map.ChronicleMap;
import net.openhft.chronicle.map.ChronicleMapBuilder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
/**
 * Created by CUONTO on 23-6-2018.
 */
@RestController
public class NumberDataStore{

    File numberFile;
    HistoricalValues historicalValues = null;
    HistoricalValues arrayWithWindowSize = null;
    boolean isCreateHitoricalValue = false;
    int passWindowSize;
    ChronicleMap<Integer, Number> numberMap;
    public NumberDataStore(){
        numberFile = new File("number.cm");
        try {
            numberMap = ChronicleMapBuilder.of(Integer.class, Number.class)
                            .name("The X Map")
                            .averageValueSize(40)
                           .maxBloatFactor(100) // If size-related exception occurs, use this to prepare for mem
// explosion
                            .entries(10_000).createOrRecoverPersistedTo(numberFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Number put(int i, Number x){
        return numberMap.put(i, x);
    }

    public Number get(int i){
        return numberMap.get(i);
    }

    public Set<Integer> getKeySet(){
        return numberMap.keySet();
    }

    @GetMapping("/currentvalues")
    @CrossOrigin(origins = "http://localhost:4200")
    public String getLastElement() {
//        System.out.println("Test calling current value");
        Set<Integer> unsortedKeys = getKeySet();
        List<Integer> list = new ArrayList<Integer>(unsortedKeys);
        java.util.Collections.sort(list);
        Number currentValues = new Number(new BigDecimal(0),new BigDecimal(0),
                new BigDecimal(0), new BigDecimal(0),new BigDecimal(0),new BigDecimal(0));
        ObjectMapper mapper = new ObjectMapper();
        try {
            currentValues = get(list.get(list.size() - 1));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            return mapper.writeValueAsString(currentValues);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    
    /**
     * Returns a serialized list of Numbers, sorted latest first, whose size determines how far you want to look
     * back in history. If windowsSize is bigger than underlying map's size, then the oldest values are padded with
     * zeros
     *
     * @param windowSize
     * @return
     */
    @GetMapping("/historicalvalues")
    @CrossOrigin(origins = "http://localhost:4200")
    public String getLastElements(@RequestParam("windowSize") String windowSize) {
        int arraySize = Integer.parseInt(windowSize);

        Set<Integer> unsortedKeys = getKeySet();
        List<Integer> list = new ArrayList<Integer>(unsortedKeys);
        System.out.println("Chronicle Map length: " + list.size());
        java.util.Collections.sort(list);
        Number numberWithAllZero = new Number(new BigDecimal(0),new BigDecimal(0),
                new BigDecimal(0), new BigDecimal(0),new BigDecimal(0),new BigDecimal(0));

        HistoricalValues arrayWithWindowSize = new HistoricalValues(arraySize);
        ObjectMapper mapper = new ObjectMapper();

        for (int i = 0; i < arraySize; i++){
            if (list.size() - 1 - i < 0){
                arrayWithWindowSize.add(numberWithAllZero);
            } else {
                arrayWithWindowSize.add(get(list.get(list.size() - 1 - i)));
            }
        }

        try {
            System.out.println("===getLastElements === " );
            return mapper.writeValueAsString(arrayWithWindowSize);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }


//    /**
//     * Returns a serialized list of Numbers, sorted latest first, whose size determines how far you want to look
//     * back in history. If windowsSize is bigger than underlying map's size, then the oldest values are padded with
//     * zeros
//     *
//     * @param windowSize
//     * @return
//     */
//    @GetMapping("/historicalvalues")
//    @CrossOrigin(origins = "http://localhost:4200")
//    public String getLastElements(@RequestParam("windowSize") String windowSize) {
//
//        /*PVViet, Create a sample hitory array with random number*/
//        //Change last element
//        int arraySize = Integer.parseInt(windowSize);
//        if (this.historicalValues == null){
//            this.historicalValues = createHistoricalValues();
//        }
//        //HistoricalValues arrayWithWindowSize;
//        if (passWindowSize != arraySize){
//            isCreateHitoricalValue = false;
//            passWindowSize = arraySize;
//            arrayWithWindowSize = new HistoricalValues(arraySize);
//            System.out.println("HOW ABOUT THS SIMPLE SOLUTION: "+arraySize);
//        }
////        if (!isCreateHitoricalValue) {
////            passWindowSize = arraySize;
////            //historicalValues = createHistoricalValues();
////            //isCreateHitoricalValue = true;
//////            arrayWithWindowSize = new HistoricalValues(arraySize);
////            System.out.println("HOW ABOUT THS SIMPLE SOLUTION: "+arraySize);
////        }
//        ObjectMapper mapper = new ObjectMapper();
//
//        int elementToIterate = historicalValues.baseToCounterPrice.length - arraySize;
//        int sizeOfHistory = historicalValues.baseToCounterPrice.length;
//        //change the last value
////        if (isCreateHitoricalValue){
////            BigDecimal max = new BigDecimal("1000.0").setScale(8, RoundingMode.HALF_EVEN);
////            BigDecimal min = new BigDecimal("-1000.0").setScale(8, RoundingMode.HALF_EVEN);
////            BigDecimal range = max.subtract(min).setScale(8, RoundingMode.HALF_EVEN);
////            BigDecimal randFromDoubleTotalProfit;
////            BigDecimal randFromDoubleDailyProfit;
////            BigDecimal randFromDoubleRelTotalProfit;
////            BigDecimal randFromDoubleRelDailyProfit;
////            BigDecimal randFromDoubleBaseToCounterPrice;
////            BigDecimal randFromDoubleCounterToUSDPrice;
////            randFromDoubleTotalProfit = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
////            arrayWithWindowSize.absTotalProfit[arraySize-1] = min.add(range.multiply(randFromDoubleTotalProfit));
////            randFromDoubleDailyProfit = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
////            arrayWithWindowSize.absDailyProfit[arraySize-1] = min.add(range.multiply(randFromDoubleDailyProfit));
////            randFromDoubleRelTotalProfit = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
////            arrayWithWindowSize.relTotalProfit[arraySize-1] = min.add(range.multiply(randFromDoubleRelTotalProfit));
////            randFromDoubleRelDailyProfit = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
////            arrayWithWindowSize.relDailyProfit[arraySize-1] = min.add(range.multiply(randFromDoubleRelDailyProfit));
////            randFromDoubleBaseToCounterPrice = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
////            arrayWithWindowSize.baseToCounterPrice[arraySize-1] = min.add(range.multiply(randFromDoubleBaseToCounterPrice));
////            randFromDoubleCounterToUSDPrice = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
////            arrayWithWindowSize.counterToUSDPrice[arraySize-1] = min.add(range.multiply(randFromDoubleCounterToUSDPrice));
////        }
//        if(!isCreateHitoricalValue) {
//            for (int j = 0, i = elementToIterate; i < sizeOfHistory; i++) {
//
//                arrayWithWindowSize.absTotalProfit[j] = historicalValues.absTotalProfit[i];
//                arrayWithWindowSize.absDailyProfit[j] = historicalValues.absDailyProfit[i];
//                arrayWithWindowSize.relDailyProfit[j] = historicalValues.relDailyProfit[i];
//                arrayWithWindowSize.relTotalProfit[j] = historicalValues.relTotalProfit[i];
//                arrayWithWindowSize.counterToUSDPrice[j] = historicalValues.counterToUSDPrice[i];
//                arrayWithWindowSize.baseToCounterPrice[j] = historicalValues.baseToCounterPrice[i];
//                j++;
//            }
//            isCreateHitoricalValue = true;
//        }
//        try {
//            return mapper.writeValueAsString(arrayWithWindowSize);
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    private HistoricalValues createHistoricalValues() {

            BigDecimal max = new BigDecimal("1000.0").setScale(8, RoundingMode.HALF_EVEN);
            BigDecimal min = new BigDecimal("-1000.0").setScale(8, RoundingMode.HALF_EVEN);
            BigDecimal range = max.subtract(min).setScale(8, RoundingMode.HALF_EVEN);
            BigDecimal randFromDoubleTotalProfit;
            BigDecimal randFromDoubleDailyProfit;
            BigDecimal randFromDoubleRelTotalProfit;
            BigDecimal randFromDoubleRelDailyProfit;
            BigDecimal randFromDoubleBaseToCounterPrice;
            BigDecimal randFromDoubleCounterToUSDPrice;

            HistoricalValues historicalValues = new HistoricalValues(50);
            for (int i = 0; i < 50; i++) {
                randFromDoubleTotalProfit = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
                historicalValues.absTotalProfit[i] = min.add(range.multiply(randFromDoubleTotalProfit));
                randFromDoubleDailyProfit = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
                historicalValues.absDailyProfit[i] = min.add(range.multiply(randFromDoubleDailyProfit));
                randFromDoubleRelTotalProfit = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
                historicalValues.relTotalProfit[i] = min.add(range.multiply(randFromDoubleRelTotalProfit));
                randFromDoubleRelDailyProfit = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
                historicalValues.relDailyProfit[i] = min.add(range.multiply(randFromDoubleRelDailyProfit));
                randFromDoubleBaseToCounterPrice = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
                historicalValues.baseToCounterPrice[i] = min.add(range.multiply(randFromDoubleBaseToCounterPrice));
                randFromDoubleCounterToUSDPrice = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
                historicalValues.counterToUSDPrice[i] = min.add(range.multiply(randFromDoubleCounterToUSDPrice));
            }
        return historicalValues;
    }

}
