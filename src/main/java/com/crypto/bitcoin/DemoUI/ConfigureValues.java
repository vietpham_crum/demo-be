package com.crypto.bitcoin.DemoUI;

import lombok.*;

@Getter @Setter
@NoArgsConstructor
@ToString @EqualsAndHashCode

public class ConfigureValues {
    String inputField;
    String selectedOption;
    String[] multiSelectedOption;
    String radio;
    CheckBox[] checkbox;

    public ConfigureValues(String inputField, String selectedOption, String[] multiSelectedOption, String radio, CheckBox[] checkbox) {
        this.inputField = inputField;
        this.selectedOption = selectedOption;
        this.multiSelectedOption = multiSelectedOption;
        this.radio = radio;
        this.checkbox = checkbox;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("************************************");
        sb.append("\ninputField: ").append(this.inputField);
        sb.append("\nsingleSelectOption: ").append(this.selectedOption);
        sb.append("\nmultiSelectedOption: ").append(this.multiSelectedOption);
        sb.append("\nradio: ").append(this.radio);
        sb.append("\ncheckbox: ").append(this.checkbox);
        sb.append("\n************************************");
        return sb.toString();
    }

    public String getInputField() {
        return inputField;
    }

    public void setInputField(String inputField) {
        this.inputField = inputField;
    }

    public String getSelectedOption() {
        return selectedOption;
    }

    public void setSelectedOption(String selectedOption) {
        this.selectedOption = selectedOption;
    }

    public String[] getMultiSelectedOption() {
        return multiSelectedOption;
    }

    public void setMultiSelectedOption(String[] multiSelectedOption) {
        this.multiSelectedOption = multiSelectedOption;
    }

    public String getRadio() {
        return radio;
    }

    public void setRadio(String radio) {
        this.radio = radio;
    }

    public CheckBox[] getCheckbox() {
        return checkbox;
    }

    public void setCheckbox(CheckBox[] checkbox) {
        this.checkbox = checkbox;
    }
}
