package com.crypto.bitcoin.DemoUI;

import org.springframework.stereotype.Component;
import org.springframework.scheduling.annotation.Scheduled;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by CUONTO on 23-5-2018.
 */
//public class ChronicleMapExampleWriter implements DisposableBean, Runnable{
@Component
public class ChronicleMapExampleWriter{
    
    public static Integer NR_SECONDS_PER_DAY = 10;
    int i = 0;
    NumberDataStore store;
    BigDecimal max = new BigDecimal("1000.0").setScale(8, RoundingMode.HALF_EVEN);
    BigDecimal min = new BigDecimal("-1000.0").setScale(8, RoundingMode.HALF_EVEN);
    BigDecimal range = max.subtract(min).setScale(8, RoundingMode.HALF_EVEN);
    BigDecimal randFromDoubleTotalProfit;
    BigDecimal absTotalProfit;
    BigDecimal randFromDoubleDailyProfit;
    BigDecimal absDailyProfit;
    BigDecimal randFromDoubleRelTotalProfit;
    BigDecimal relTotalProfit;
    BigDecimal randFromDoubleRelDailyProfit;
    BigDecimal relDailyProfit;
    BigDecimal randFromDoubleBaseToCounterPrice;
    BigDecimal baseToCounterPrice;
    BigDecimal randFromDoubleCounterToUSDPrice;
    BigDecimal counterToUSDPrice;
    Number currentValues;
    @Scheduled(fixedRate = 1000)
    public void createChronicleMapFile() {
            store = new NumberDataStore();
            randFromDoubleTotalProfit = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
            absTotalProfit = min.add(range.multiply(randFromDoubleTotalProfit));
            randFromDoubleDailyProfit = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
            absDailyProfit = min.add(range.multiply(randFromDoubleDailyProfit));
            randFromDoubleRelTotalProfit = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
            relTotalProfit = min.add(range.multiply(randFromDoubleRelTotalProfit));
            randFromDoubleRelDailyProfit = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
            relDailyProfit = min.add(range.multiply(randFromDoubleRelDailyProfit));
            randFromDoubleBaseToCounterPrice = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
            baseToCounterPrice= min.add(range.multiply(randFromDoubleBaseToCounterPrice));
            randFromDoubleCounterToUSDPrice = new BigDecimal(Math.random()).setScale(8, RoundingMode.HALF_EVEN);
            counterToUSDPrice = min.add(range.multiply(randFromDoubleCounterToUSDPrice));
            currentValues = new Number(absTotalProfit, absDailyProfit, relTotalProfit,
                    relDailyProfit, baseToCounterPrice, counterToUSDPrice);
            store.put(i++ / NR_SECONDS_PER_DAY, currentValues);
            System.out.println("Iterate number: " + i);
        }
    }

