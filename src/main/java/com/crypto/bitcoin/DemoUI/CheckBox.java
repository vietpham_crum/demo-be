package com.crypto.bitcoin.DemoUI;

public class CheckBox {
    String label;
    boolean select;
    public  CheckBox(){
    }
    public CheckBox(String label, boolean select) {
        this.label = label;
        this.select = select;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }
}
