package com.crypto.bitcoin.DemoUI;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class HistoricalValues implements Serializable {
    BigDecimal [] absTotalProfit;
    BigDecimal [] absDailyProfit;
    BigDecimal [] relTotalProfit;
    BigDecimal [] relDailyProfit;
    BigDecimal [] baseToCounterPrice;
    BigDecimal [] counterToUSDPrice;
    int arrayLength = 0;
    int currentIndex = -1;
    
    public HistoricalValues(int arrayLength){
        this.absTotalProfit = new BigDecimal[arrayLength];
        this.absDailyProfit = new BigDecimal[arrayLength];
        this.relTotalProfit = new BigDecimal[arrayLength];
        this.relDailyProfit = new BigDecimal[arrayLength];
        this.baseToCounterPrice = new BigDecimal[arrayLength];
        this.counterToUSDPrice = new BigDecimal[arrayLength];
        this.arrayLength = arrayLength;
        this.currentIndex = 0;
    }
    
    public void add(Number number){
        if (currentIndex == this.arrayLength){
            return;
        }
        this.absTotalProfit[currentIndex] = number.absTotalProfit;
        this.absDailyProfit[currentIndex] = number.absDailyProfit;
        this.relTotalProfit[currentIndex] = number.relTotalProfit;
        this.relDailyProfit[currentIndex] = number.relDailyProfit;
        this.baseToCounterPrice[currentIndex] = number.baseToCounterPrice;
        this.counterToUSDPrice[currentIndex] = number.counterToUSDPrice;
        currentIndex++;
    }
}

