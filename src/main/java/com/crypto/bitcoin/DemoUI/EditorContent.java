package com.crypto.bitcoin.DemoUI;

public class EditorContent {
    String textValue;
    public  EditorContent(){

    }
    public EditorContent( String textValue){
        this.textValue = textValue;
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("************************************");
        sb.append("\ntextValue: ").append(this.textValue);
        sb.append("\n************************************");
        return sb.toString();
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }
}
