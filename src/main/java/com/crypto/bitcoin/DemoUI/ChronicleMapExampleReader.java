package com.crypto.bitcoin.DemoUI;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by CUONTO on 23-5-2018.
 */
public class ChronicleMapExampleReader {

    public static void main(String[] args) {
        NumberDataStore store = new NumberDataStore();

        /**
         * Initially reading is "slower" than writing because printing to console blocks the reader from reading very fast
         * , then due to the writer being slowed down (waiting 1 second after each write), then reading catches up with writing
         */
        while (true) {

            Set<Integer> unsortedKeys = store.getKeySet();

            List<Integer> list = new ArrayList<Integer>(unsortedKeys);
            java.util.Collections.sort(list);

            try {
                String x = store.getLastElement();
//                System.out.println("Latest entry, key: " + (list.size() - 1) + ", Total Profit: " + x.getAbsTotalProfit()
//                + ", Daily profit: " + x.getAbsDailyProfit() + ", Base to counter price: " + x.getBaseToCounterPrice()
//                + ", Counter to USD price: " + x.getCounterToUSDPrice() + ", Relative total profit: " + x.getRelTotalProfit()
//                + ", Relative daily profit: " + x.getRelDailyProfit());
                System.out.println(x);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
