package com.crypto.bitcoin.DemoUI;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
public class ConfigurationContent {

    @RequestMapping(value="/configure",  method=RequestMethod.POST)
    @CrossOrigin(origins = "http://localhost:4200")
    public void createTextContentFile(@RequestBody String content) {
        System.out.println("TEXT AREA CONTENT: " + content);
        Path file = Paths.get("tradersettings.properties");
        try {
            Files.write(file, content.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @RequestMapping(value="/configure",  method=RequestMethod.GET)
    @CrossOrigin(origins = "http://localhost:4200")
    public ConfigureValues getConfigureValue() {
        ConfigureValues currentValues = new ConfigureValues(new String(""),new String(""), new String[]{},
                new String(""), new CheckBox[]{});
        ObjectMapper mapper = new ObjectMapper();
        Path path = Paths.get("tradersettings.properties");
        String fileContent = "";
        try {
            fileContent = new String(Files.readAllBytes(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            return mapper.readValue(fileContent, ConfigureValues.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    @RequestMapping(value="/editor",  method=RequestMethod.POST)
    @CrossOrigin(origins = "http://localhost:4200")
    public void createEditorContentFile(@RequestBody String content) {
        Path file = Paths.get("tradersettings.properties.editor");
        try {
            Files.write(file, content.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @RequestMapping(value="/editor",  method=RequestMethod.GET)
    @CrossOrigin(origins = "http://localhost:4200")
    public EditorContent getEditorValue() {

        ObjectMapper mapper = new ObjectMapper();
        Path path = Paths.get("tradersettings.properties.editor");
        String fileContent = "";
        try {
            fileContent = new String(Files.readAllBytes(path));
            //return fileContent;
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            EditorContent tmf = mapper.readValue(fileContent, EditorContent.class);
            System.out.println(tmf);
            return mapper.readValue(fileContent, EditorContent.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
